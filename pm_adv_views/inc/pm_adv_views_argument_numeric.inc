<?php
// $Id:

/**
 * @file pm_adv_views_argument_numeric.inc
 *
 * Provides the default argument handler.
 */
class pm_adv_views_argument_numeric extends views_handler_argument_numeric {
  function query() {
    $this->ensure_my_table();

    if (!empty($this->options['break_phrase'])) {
      views_break_phrase($this->argument, $this);
    }
    else {
      $this->value = array($this->argument);
    }

    if (count($this->value) > 1) {
      $operator = empty($this->options['not']) ? 'IN' : 'NOT IN';
      $placeholders = implode(', ', array_fill(0, count($this->value), '%d'));

      $this->query->add_where(NULL, "users.uid $operator ($placeholders)", $this->value);
    }
    else {
      $operator = empty($this->options['not']) ? '=' : '!=';

      $this->query->add_where(NULL, "users.uid = %d", $this->argument);
    }
  }
}
