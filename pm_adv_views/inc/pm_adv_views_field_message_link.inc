<?php
// $Id:

/**
 * @file pm_adv_views_field_message_link.inc
 */

/**
 * Views field handler for the Privatemsg advanced views write message link.
 *
 * @ingroup views
 */
class pm_adv_views_field_message_link extends views_handler_field {
  function query() {
    global $user;

    $this->ensure_my_table();

    // To exclude anonymous user.
    if (!isset($this->view->field['status'])) {
      $this->query->add_field('users', 'status');
    }

    $sql = "SELECT COUNT(recipient) FROM {pm_block_user} WHERE author = users.uid AND recipient = ". $user->uid;
    $this->field_alias = $this->query->add_field(NULL, "(". $sql .")", 'blocked');

    $this->query->add_field('users', 'uid');

    $this->query->add_where($this->options['group'], 'users.uid <> 0');
  }

  function option_definition() {
    $options = parent::option_definition();

    $options['subject'] = array('default' => '', 'translatable' => TRUE);
    $options['combination'] = array('default' => '', 'translatable' => TRUE);

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['subject'] = array(
      '#type' => 'textfield',
      '#title' => t('Pre-filled subject'),
      '#default_value' => $this->options['subject'],
      '#description' => t('Define the subject that will be pre-filled in the send message form. You can use replacement tokens to insert any existing field output.'),
    );

    $description = t('Display of both links - the link to block/unblock and the link to write a message.') .'<br />';
    $description .= t('This is good, considering it under the performance aspect.');
    $form['combination'] = array(
      '#type' => 'checkbox',
      '#title' => t('Link combination'),
      '#default_value' => $this->options['combination'],
      '#description' => $description,
    );

    $i = 0;
    foreach ($form as $key => $element) {
      if (substr($key, 0, 1) == '#') {
        continue;
      }
      $form[$key]['#weight'] = $i;
      ++$i;
    }
    $form['subject']['#weight'] = 2.5;
    $form['combination']['#weight'] = 2.7;
  }

  function element_type() {
    return 'div';
  }

  function render($values) {
    if (isset($values->uid)) {
      $uid = $values->uid;
    }
    elseif (isset($values->users_uid)) {
      $uid = $values->users_uid;
    }
    else {
      return FALSE;
    }

    if ($this->options['combination'] == FALSE && $values->blocked == 1) {
      return FALSE;
    }

    $subject = NULL;
    if (!empty($this->options['subject'])) {
      $tokens = $this->get_render_tokens($this);
      $subject = strip_tags(strtr($this->options['subject'], $tokens));
    }

    // Link combination.
    if ($this->options['combination'] == TRUE) {
      // User not blocked.
      if ($values->blocked == 0) {
        $link = '<div class="link-combination">';
        $link .= '<div class="views-field-pm-adv-block-link pm-adv-block-link">'. privatemsg_adv_create_link($op = 'block', $uid) .'</div>';
        // Last parameter FALSE: Here is a check not required if the user is blocked.
        $link .= '<div class="views-field-pm-adv-message-link pm-adv-message-link">'. privatemsg_adv_create_link($op = 'message', $uid, $subject, FALSE) .'</div>';
        $link .= '</div>';
      }
      // User blocked.
      if ($values->blocked == 1) {
        $link = '<div class="views-field-pm-adv-unblock-link pm-adv-unblock-link">'. privatemsg_adv_create_link($op = 'unblock', $uid) .'</div>';
      }
    }
    // Single link.
    else {
      // Last parameter FALSE: Here is a check not required if the user is blocked.
      $link = privatemsg_adv_create_link($op = 'message', $uid, $subject, FALSE);
    }
    
    return $link;
  }
}
