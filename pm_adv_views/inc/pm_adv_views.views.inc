<?php
// $Id:

/**
 * @file pm_adv_views.views.inc
 */

/**
 * Implementation of hook_views_data()
 */
function pm_adv_views_views_data() {
  // Link to write a private message. (virtual field)
  $help['message_link'] = t('Provides a link to write a private message.') .'<br />';
  $help['message_link'] .= t('If the user is blocked by the current user then does not display the link.') .'<br />';
  $help['message_link'] .= t('Note: It takes no additional filter to exclude anonymous users.') .'<br />';
  $help['message_link'] .= t('Attention') .'<br />';
  $help['message_link'] .= t('This link can be displayed as a link combination. When using this option, no need to use the link "Block or unblock messages".');
  $tables['users']['pm_adv_message_link'] = array(
    'group' => t('Privatemsg advanced'),
    'title' => t('Write private message'),
    'help' => $help['message_link'],
    'field' => array(
      'handler' => 'pm_adv_views_field_message_link',
    ),
    'argument' => array(
      'handler' => 'pm_adv_views_argument_numeric',
    ),
  );
  // Link to block or unblock a user. (virtual field)
  $help['ignore_link'] = t('Provides a link to block/unblock a user to write private messages.') .'<br />';
  $help['ignore_link'] .= t('Note: It takes no additional filter to exclude anonymous users.') .'<br />';
  $tables['users']['pm_adv_ignore_link'] = array(
    'group' => t('Privatemsg advanced'),
    'title' => t('Block or unblock messages'),
    'help' => $help['ignore_link'],
    'field' => array(
      'handler' => 'pm_adv_views_field_ignore_link',
    ),
    'argument' => array(
      'handler' => 'pm_adv_views_argument_numeric',
    ),
  );

  return $tables;
}

/**
 * Implementation of hook_views_handlers().
 */
function pm_adv_views_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'pm_adv_views') . '/inc'
    ),
    'handlers' => array(
      'pm_adv_views_field_message_link' => array(
        'parent' => 'views_handler_field',
      ),
      'pm_adv_views_argument_numeric' => array(
        'parent' => 'views_handler_argument_numeric',
      ),
      'pm_adv_views_field_ignore_link' => array(
        'parent' => 'views_handler_field',
      ),
    )
  );
}
