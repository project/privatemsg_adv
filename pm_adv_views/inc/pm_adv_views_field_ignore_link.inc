<?php
// $Id:

/**
 * @file pm_adv_views_field_ignore_link.inc
 */

/**
 * Views field handler for the Privatemsg advanced views block/unblock link.
 *
 * @ingroup views
 */
class pm_adv_views_field_ignore_link extends views_handler_field {
  function query() {
    global $user;

    $this->ensure_my_table();

    // To exclude anonymous user.
    if (!isset($this->view->field['status'])) {
      $this->query->add_field('users', 'status');
    }

    $sql = "SELECT COUNT(recipient) FROM {pm_block_user} WHERE author = users.uid AND recipient = ". $user->uid;
    $this->field_alias = $this->query->add_field(NULL, "(". $sql .")", 'blocked');

    $this->query->add_field('users', 'uid');
    
    $this->query->add_where($this->options['group'], 'users.uid <> 0');
  }

  function element_type() {
    return 'div';
  }

  function render($values) {
    global $user;

    if (isset($values->uid)) {
      $uid = $values->uid;
    }
    elseif (isset($values->users_uid)) {
      $uid = $values->users_uid;
    }
    else {
      return FALSE;
    }

    if (!$values->users_status) {
      return FALSE;
    }

    // Here is a check not required if the user is blocked.
    // So use the two operators block and unblock.
    // User not blocked.
    if ($values->blocked == 0) {
      $link =  privatemsg_adv_create_link($op = 'block', $uid);
    }
    // User blocked.
    if ($values->blocked == 1) {
      $link =  privatemsg_adv_create_link($op = 'unblock', $uid);
    }

    return $link;
  }
}
