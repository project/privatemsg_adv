
Privatemsg advanced - extends the Private messages core module.

Requirements
--------------------------------------------------------------------------------
- The modules are written for Drupal 6.0+.

- The Privatemsg module.
- The included Privatemsg advanced views module need the Views module.

Installation
--------------------------------------------------------------------------------
Copy the Privatemsg advanced module folder to your modules directory and then
enable the two modules

- Privatemsg advanced
- Privatemsg advanced views

on the admin modules page.

If used version 2 of the Privatemsg core module must be enabled its submodule
Block user messages.

FAQ
--------------------------------------------------------------------------------
What exactly makes Privatemsg advanced?

  At first nothing.

How can I use Privatemsg advanced?

  Please read usage points 1. and 3..

How can I use Privatemsg advanced views?

  You can create different views displays to display the links
  - Write private message
  - Block/unblock messages

  Please read usage point 2.

Usage
--------------------------------------------------------------------------------
1. The Privatemsg advanced main module provides two functions.

   - privatemsg_adv_user_blocked()

     Checks if an user is blocked.

   - privatemsg_adv_create_link()

     Displays a link to write a message or displays a link to block/unblock
     a user to write messages.

   Please read the included code comments of the file privatemsg_adv.module
   for more informations.

2. Submodule Privatemsg advanced views

   Be used at the same time the two links "Write private message" and
   "block/unblock messages" it is possible to use the link
   "Write private message" as a link combination.
   This results in a performance benefit because one database query per indicated
   user is carried out less.

3. Author Pane module

   To use this compatibility please add the following code in the file
   author-pane.tpl.php, included in the current theme folder:

   <?php /* Privatemsg advanced */ ?>
   <?php if (isset($privatemsg_adv_links)): ?>
     <div class="author-pane-line author-pm-adv-links">
       <?php print $privatemsg_adv_links; ?>
     </div>
   <?php endif; ?>

Maintainer
--------------------------------------------------------------------------------
Quiptime Group
Siegfried Neumann
www.quiptime.com
quiptime [ at ] gmail [ dot ] com
