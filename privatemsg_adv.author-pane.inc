<?php

/**
 * @file
 * Author Pane module compatibility.
 *
 *   Provides the link combination of the two links
 *   "Write private message" and the block/unblock link.
 */

/**
 * Implementation of hook_preprocess_author_pane().
 */
function privatemsg_adv_preprocess_author_pane(&$variables) {
  $variables['privatemsg_adv_links'] = _privatemsg_adv_atp_links($variables['account']->uid);
}

/**
 * Implementation of hook_author_pane_allow_preprocess_disable().
 */
function privatemsg_adv_author_pane_allow_preprocess_disable() {
  return array('privatemsg_adv' => 'Privatemsg advanced');
}

/**
 * Build the two PM adv links.
 *
 * @param int $recipient_uid
 *   The user ID to get the links.
 * @return string
 *   The rendered links.
 */
function _privatemsg_adv_atp_links($recipient_uid) {
  $links = '';
  $link_message = FALSE;
  
  if ($link_message = privatemsg_adv_create_link('message', $recipient_uid)) {
    $links = '<div class="author-pane-line pm-adv-message-link">'. $link_message .'</div>';
  }
  if ($link_message) {
    $links .= '<div class="pm-adv-block-link">'. privatemsg_adv_create_link('block', $recipient_uid) .'</div>';
  }
  else {
    $links .= '<div class="pm-adv-unblock-link">'. privatemsg_adv_create_link('unblock', $recipient_uid) .'</div>';
  }

  return $links;
}